---
title: Hele Dag
date: 2020-03-18 16:02:00 +01:00
position: 2
---

### Nieuwkoopse plassen

#### Tijd: Rondje, 8 uur varen

**Vaarkaarten**

[Klik hier voor Nieuwkoopse plassen kaart](/uploads/route%20Nieuwkoopse%20plassen%20De%20Scheepsjongens.pdf)

**Brug en Sluistijden:**

1. Sluis Slikkendam Woerdenseverlaat

 10.00 - 12.00
 13.00 - 17.00
 18.00 - 20.00

2. Nieuwkoopse plassen (neem €5,- tol mee voor natuurbeheer)

3. Ziende sluis bij Zwammerdam 9.00-19.00 uur

4. Sluis bodegraven 9.00-19.00 uur

**Beschrijving:**

Voor wie van rust en natuur houdt, is dit een mooie groene route langs de rietkragen.

Je vaart over de Grecht  naar de Nieuwkoopse plassen.
Vervolgens vaar de plassen overen ga je richting Zwammerdam.
 Over de Rijn via Boegraven en Nieuwerbrug weer terug. 
  Let wel onze sloepen varen **overal 6 km /uur** en het is 8 vaaruren.

OHWL
**Het is ook  de Oude Hollandse Waterlinie route; De zgn. "Luxembourg vaarroute". Deze is te zien via onderstaande link .

Het is eigenlijk een route die wat meer tijd kost dan 1 vaardag,  
De route is oorspronkelijk via de Meije, dat is een klein riviertje met heel veel bruggetjes die allemaal open moeten met de hand.
In plaats daarvan vaar je over de Nieuwkoopse plassen.

In een dag kun je dan dus (meest van) deze route doen.
Dat is bij ons de bovenstaande "Nieuwkoopseplassen vaarroute".

De informatie van OHWL is heel interessant bij deze route.   
(Na punt 7 ga je naar punt 11, (dus niet afslaan naar 8 maar vaar je een stukje rechtdoor), dan de plassen op en daar pak je de route weer op. 

[https://oudehollandsewaterlinie.nl/routes/de-luxembourg-vaarroute/](https://oudehollandsewaterlinie.nl/routes/de-luxembourg-vaarroute/)


### Linschoten, Oudewater, Montfoort

#### Tijd: 6 uur varen, 2 uur vrije tijd

**Vaarkaarten:**

[Klik hier voor Rondje Oudewater](/uploads/rondje%20Oudewater%20-%20Montfoort.pdf)

**Brug en Sluistijden:**

1. Sluis Oudewater:

2. Deze sluis draait op zelfbediening.
   Vaar de sloep tot in de sluis, volg de instructies ter plaatse,
   Druk op de knop en dan moet je **even geduld** hebben, bij te vaak drukken kan er storing ontstaan;
   Sloep niet vastbinden  maar het touw evt laten vieren ivm stijgen/ dalen van het water; je kan de haakstok gebruiken. en houd handen binnenboord!

Sluis Montfoort wordt bediend,
Je kan het nr bellen dat op het bord staat kost een paar euro.

**Beschrijving:**

Populaire en leuke route langs Historische dorpen / stadjes, Linschoten, Montfoort en Oudewater.

### Rondje Oudewater, Hekendorp , Nieuwerbrug.
#### Tijd: Rondje, 8 uur varen



vaarkaart: [route Hekendorp De Scheepsjongens.pdf](/uploads/route%20Hekendorp%20De%20Scheepsjongens.pdf)


Deze ronde is 8 vaar uren, je komt 2 x een sluis tegen en een spoorbrug op de dubbele Wiericke 
let op :deze spoorbrug  heeft beperkte openingstijden.
Wordt op zondag alleen geopend van 14.00-16.00 uur!
en door de week ook op strakke tijden.

**De Route Prins willem III van de OHWL is ook deze route, alleen daar ga je, ipv naar Gouda, nu bij Hekendop rechtsaf, de Dubbele Wiericke op, daardoor is deze hele route in 1 dag te doen.

Hieronder de link, met leuke en interessante informatie. 

Deze kan je lezen/luisteren als je deze route over Hekendorp- Nieuwerbrug vaart. Bij punt 13 sla je rechtsaf naar punt 17
(14,15 en 16 sla je even over met varen als je maar 1 dag gaat) 

(https://oudehollandsewaterlinie.nl/routes/de-prins-willem-iii-vaarroute/)
