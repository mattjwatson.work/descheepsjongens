---
title: Halve Dag
date: 2020-03-18 16:02:00 +01:00
position: 1
---

Rondom Woerden kun je mooi varen;  Er is veel te zien en je ervaart de omgeving van een hele andere kant.

Om de stad Woerden heen varen door de singels is mooi...
Maar een heel rondje om de stad kan helaas niet, 
Bij de Oostdam is de brug massief en de brug bij het kasteel is te laag; Je vaart dan dus weer de andere kant op terug.

We hopen dat de gemeente dit een keer mogelijk maakt want dat zou voor veel mensen en de stad in zijn geheel heel leuk zijn.

Let op:  In een Halve dag vaar je **tot** de sluizen en niet er doorheen! 


### Harmelen
#### Tijd: Heen en weer, 2,5 uur

[Klik hier voor de routekaart naar Harmelen](/uploads/route%20harmelen.pdf)

**Brug en Sluistijden:**

Alle bruggen zijn hoog genoeg voor de sloep om eronderdoor te kunnen. Bereid jullie wel voor om te bukken! 

De hofbrug,(blauwe brug vlak voor Harmelen) kan je voorzichtig zachtjes onderdoor varen;  Vaar door de meest linkse kant als je uit Woerden komt, dit is de hoogste doorgang.(wel bukken)

De Haanwijker sluis wordt maar om de 2 uur bediend, op afroep.
dit is in een halve dag niet haalbaar,

**Beschrijving:**

Het is een mooie, rustige en groene route naar het dorpje Harmelen. Je vaart over de Oude Rijn en dat was vroeger de Romijnse Limes, de Noordgrens van het Romeinse rijk; In die tijd was dit water een stuk breder.

Naast de Oude Rijn loopt het Jaagpad van Woerden tot Harmelen.

Je kan de sloep bij de sluis draaien of je kan de boot aanleggen voor de sluis en een stukje gaan wandelen;

Vanaf de sluis en loop je over het jaagpad in ong 7 minuten naar het centrum, of je loopt over de Hollandse kade naar natuurgebied De Kievit.

Door de vele bomen langs het water, is er op warme dagen ook wat schaduw.


### Linschoten, Oudewater
#### Tijd: Heen en weer, 4,5 uur

**Vaarkaarten:**

[Klik hier voor de route kaart naar Oudewater](/uploads/route%20Oudewater%20De%20Scheepsjongens.pdf)

**Brug en Sluistijden:**

In een halve dag kun je ivm de tijd wat het kost, **niet** door de sluis in Oudewater. (ook is er regelmatig een storing waardoor je er soms niet snel meer doorheen terug komt)


**Beschrijving:**
In al deze mooie oude dorpjes/stadjes is er de mogelijkheid om aan te leggen voor een korte pauze. Let wel goed op de tijd, en keer op tijd om om op de afgesproken tijd terug te zijn.

In Linschoten is het vrij smal dus vaar rustig en voorzichtig;
er zijn daar lekkere ijsjes te koop trouwens ...

### Nieuwerbrug, Bodegraven
#### Tijd: Heen en weer, 4 uur

**Vaarkaarten**

[Klik hier voor de routekaart naar Bodegraven](/uploads/route-Bodegraven-De-Scheepsjongens.pdf)

**Brug en Sluistijden: Brug Bodegraven (Vrije doorgang)**

**Beschrijving:**

De Rijn is hier wat breder en dat vaart makkelijk.
Voorheen heette deze route de Romeinse Limes en dit was de echte noordgrens van het Romeinse Rijk.


### Montfoort
#### Tijd: Heen en weer, 3 uur
[Klik hier voor de routekaart naar Montfoort](/uploads/route%20Montfoort%20De%20Scheepsjongens.pdf)


**Brug en Sluistijden:**

De sluis in Montfoort wordt op afroep, door vrijwilligers, bediend; De sluis doorgaan kost wel wat tijd, en er wordt heen én terug een bijdrage gevraagd; 

**Beschrijving:**

Geniet van een mooie, rustige en groene route door de weilanden naar het dorpje Montfoort, je kan voor de sluis aanleggen of als je meer tijd hebt net na de sluis rechtsaf, aan de overkant is ook een steigertje.

### Woerdense verlaat over de Grecht
#### Tijd: Heen en weer, 4,5 uur

**Beschrijving:**

Deze route is door een rustig landelijk gebied, als je tegenover de Haven de brug onderdoor gaat, dan vaar je zo Woerden uit, bij de eerst volgende afslag, bij de Blokhuisbrug, ga je naar rechts de Greft op. Halverwege de tijd keer je om, dat is ongeveeer bij de sluis in Woerdense Verlaat.



 
