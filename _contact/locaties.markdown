---
title: Locaties
date: 2020-03-18 12:25:00 +01:00
---

### Woerden

Ons verhuuradres in Woerden bevind zich aan de kade tussen Singel 64 en 65. 

Er zijn gratis parkeerplaatsen langs de Singel beschikbaar.
max. 2 plekken per sloep; 

Vanaf bus- en treinstation Woerden is het 10 minuten lopen. ( kom niet met het openbaar vervoer ivm het corona virus)

### Harmelen

Voor vaartochten vanuit Harmelen, kunt u terecht op Haanwijk

Hier zijn parkeerplekken beperkt, dus kom zo veel mogelijk op de fiets.

Cadeaubonnen kunnen op bestelling opgehaald worden in Harmelen of Woerden na afspraak.
