---
title: 3. Aandachtspunten
date: 2020-03-18 12:25:00 +01:00
---

1) Pas op voor zwemmers en de hengels van vissers! ze zijn soms moeilijk te zien in het water. houdt afstand! 

2) De huurder moet er voor zorg dragen dat er **géén afval** in de natuur terecht komt.  ~Afval zelf mee naar huis nemen.

3) De sloep a.u.b. netjes achter laten: Een doekje en stoffer en blik en een doekje zijn beschikbaar bij terugkomst.  **Wij** willen het voor je doen,  dan wordt **€ 10,- **verrekend met de **borg.**

4) **Hond** liever niet, maar in overleg eventueel mogelijk. Kosten: **€ 10,- extra.** De hond mag niet op de kussens, en op een eigen kleedje. 

5) De kappen zijn netjes naar beneden gevouwen, **graag zo houden.**  * Alleen bij regen eventueel te gebruiken. - De kap moet altijd **binnenboord** opgevouwen zijn (i.v.m. beschadigen)

 *  Met zon is het net zo warm onder de kap,  je hebt weinig zicht,  sommige bruggen kan je zo **niet** onderdoor, dus beter zo te laten

6) **Touwen binnenboord**! Anders komen ze in de schroef terecht en draait de **motor stuk.**

7) Als de sloep iets **in de schroef** lijkt te hebben en niet goed vaart, dan kun je de sloep even in z’n **achteruit** zetten. Meestal draait het weer los. Zo niet, **motor uitzetten!** en **ons bellen** om te voorkomen dat de motor in de soep draait!

8) De **hendel/joystick **van de stille sloepen is gevoelig, Pas op! als je de hendel **aanstoot** of er langs loopt, anders **schiet de sloep naar voor(of achter).**
Bij uitstappen daarom de sloep **uitzetten** met de sleutel.

9) Als je door een **sluis of brug** vaart, moet er goed rekening gehouden worden met de **openingstijden**:   ~sluizen en bruggen worden soms op vaste tijden geopend. Let goed op de laatste doorvaar mogelijkheid en dat je de **goede route** vaart

10) Er wordt  bij sluizen en bruggen vaak een paar euro **tol **gevraagd en bij de Nieuwkoopse plassen een bijdrage voor natuurbeheer

11) We onderhouden de sloepen goed en het gebeurd niet vaak, maar een technische storing of **pech** **kán** voorkomen, dat is overmacht. We proberen het dan uiteraard zo goed mogelijk op te lossen. We vragen dan ook jullie begrip hiervoor. 

12) Niet op de rand/ punt zitten van de boot, dat is gevaarlijk.
    ook iets achter de boot hangen is niet toegestaan.

13) Er gelden voorlopig **nieuwe extra regels** ivm het corona virus die te lezen zijn bij nieuws / maatregelen mbt het corona virus / **corona update**. Indien wij als onderneming een boete krijgen omdat er ondanks onze regels iets niet wordt nageleefd wordt dit verhaald op de huurder.

14) Als het slecht weer is kan de vaartocht **in overleg met ons op de dag zelf ** kosteloos verplaatst of geannuleerd worden; 
let wel:

Indien wij daar speciaal naar het vertrekpunt komen en er wordt beslist niet te varen, dan rekenen we de gemaakte kosten. Het kan het hele dagdeel zijn of service kosten, dit voor ons om te bepalen.

15) Als de reservering gemaakt is gaat de huurder **akkoord** met **al deze voorwaarden.**
