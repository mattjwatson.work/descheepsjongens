---
title: 2. Schade
date: 2020-03-18 12:25:00 +01:00
---

De Scheepsjongens zijn **niet aansprakelijk** voor geleden schade van de huurder;  het zij persoonlijk letsel of materiële schade, opgelopen in de verhuurperiode. Check  evt. bij je eigen verzekering of  je dit **goed** geregeld hebt.  
 
1) De sloep wordt in de **zelfde staat** terug gebracht als dat deze bij aanvang was. 

2) Als de sloep in huurperiode ergens **tegenaan** gekomen is of andere bijzonderheden of schade moet dit **ALTIJD gemeld** worden.
Als er **rustig** gevaren wordt en goed **opgepast** wordt in de buurt van **kades, sluizen en bruggen** kun je schade meestal voorkomen. **Remmen is hendel achteruit**. Met de afzetstok kun je schades helpen voorkomen.

3) In sluizen de sloep **niet vastbinden**; Niet samen met een andere boot onder een brug i.v.m. golfslag.

4) Bij aanvang wordt € 150,- borg en €10,- schoonmaakborg extra betaald, dat mits de sloep **onbeschadigd en netjes** gebleven is, **terug** ontvangen wordt.

5) Eventuele **schade** aan de sloep, kussenset, kap .ed. of aan derden wordt **verhaald op de huurder**. 
Kleine beschadigingen worden van de borg ingehouden.
Grotere schade gaat via de verzekering, Het eigen risico bedrag is €350,-

6) Nader kanten heel **voorzichtig** en zorg daarom dat bij aanmeren altijd de **stootwillen** tussen wal en t schip hangen ! gebruik de **haakstok**.

7) **Pas op met roken** in de boot (ivm brandgaatjes) en scherpe voorwerpen in en aan kleding;
en **zonnebrand crème** maakt **akelige vlekken** op de kussens die er niet meer uit gaan, dus pas op! Neem hiervoor een **handdoek** mee om op te zitten.

 **Lenen is gratis :)**
Bij verlies worden de volgende kosten van de borg ingehouden:
1. Stootwil: €30,-
2. Afzetstok: €30,-
3. Zwemvest: €35,-
4. Paraplu: €12,50
5. Vaarkaart: €5,-
