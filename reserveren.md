---
title: Reserveren
permalink: "/reserveren"
soldout: '27/03/2020

'
electricsoldout: '09/05/2020'
buitenboordsoldout: '08/05/2020'
layout: reserveren
---

Voor je reserveert, lees éérst de voorwaarden* goed door:

[Klik **hier** voor de voorwaarden. (opent in nieuw venster)](http://descheepsjongens.nl/voorwaarden)

*Als de reservering is gemaakt gaat u akkoord met onze voorwaarden.

### Beschikbaarheid:

**Zaterdag 18 juli en zondag 19 juli** zijn de sloepen in de middag allemaal gereserveerd in Woerden en Harmelen.
Er zijn nog sloepen vrij in de ochtend en de avond**

Reserveer eenvoudig door je gegevens en wensen in te vullen op het formulier hieronder.

Wanneer de reservering is gemaakt sturen we je de bevestiging met de informatie die je nodig hebt, en dan verwachten we een **ontvangstbevestiging/reply terug** of alles duidelijk is; Dan is de reservering compleet.