---
title: Prijzen
permalink: "/prijzen"
layout: prijzen
---

Alle sloepen hebben de zelfde prijs, electrische sloepen en benzine motorsloepen,

### Ochtend €85,- 

#### 10:00 - 13:00 (3 uur)

### Middag €100,-

#### 13:30 - 18:00 (4,5 uur)

###  Avond €85,-

#### 18:30 - 21:00 (2,5 uur)

### Hele Dag €170,-

#### 10:00 - 18:00 (8 uur) 




### De Borg is €160,-  dat is inclusief €10,- schoonmaakborg.

De sloep hebben we namelijk graag weer in de zelfde staat terug als dat deze was bij aanvang. Dan krijg je de borg ook weer terug.

Bij terugkomst kun je zelf de sloep weer schoon maken;

Wij willen het ook  voor je doen, dan houden we de €10,- schoonmaakborg in.

~ Kleine schade en onkosten worden van de borg afgehouden.

~ Grotere schade loopt via de verzekering.
De huurder is verantwoordelijk voor de eerste €350,- eigen risico.

We rekenen geen reserveringskosten. 
Annuleren als het regent is kosteloos , kan op de dag zelf, in overleg met ons =/- een uur voor de vertrek tijd. 

Indien wij daar speciaal heen gegaan zijn en de vaartocht gaat niet door of je komt niet opdagen, wordt óf het hele huurbedrag  gerekend óf €20,- service onkosten gerekend, dit door ons te bepalen.

~ Een extra uur bij huren is soms mogelijk,dit in overleg, €25,-per uur.  Vooraf te reserveren.

~Cadeaubonnen zijn te bestellen via reservering formulier of een email.
we hebben cadeaubonnen voor:
- Hele dag €170,-
- Halve dag €100,-
- of een bedrag naar wens.

