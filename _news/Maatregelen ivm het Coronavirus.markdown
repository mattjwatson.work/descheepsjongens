---
title: Maatregelen ivm het Coronavirus
date: 2020-03-18 12:25:00 +01:00
---


We houden het nieuws nauwlettend in de gaten en we houden ons aan de richtlijnen van het RIVM en de overheid. 

Vanaf 1 juli zijn de regels weer wat soepeler en mogen er een paar meer mensen op de sloep; 
Maar alleen als je je aan de regels van het RIVM/overheid houdt én er 1.5 meter afstand gehouden wordt tussen personen 18+(indien je niet samenwoont) 

De regels mbt het Coronavirus:

1. ~Volg de richtlijnen van het RIVM
2. ~ Voelt u zich niet lekker of bent u verkouden of heeft u koorts, ga er niet op uit, blijf thuis!
3. ~ Als je onderweg aanlegt of uitstapt houdt je aan de regels, neem zelf je picknickmand mee, of bestel deze bij een lokaal restaurant; dat is nog leuker!
4. ~ Kom niet met het openbaar vervoer.
5. ~ Koppels/gezinnen die samen wonen mogen samen in de sloep, volwassenen moeten nog steeds 1,5 m afstand houden van elkaar

wat kan:
 bv 
max 3 "afzonderlijke"  personen of 2 samenwonende koppels in de Antaris** 
of 
max 5 "afzonderlijke" personen of 3 samenwonende koppels in de electrische sloepen

6. ~ Bij vertrek zal de uitleg buiten en op gepaste afstand worden gegeven.  

7. ~ Zoals altijd maken we de boten voor en na iedere vaart schoon.
De sloepen worden nu extra gedesinfecteerd en u kunt zelf gebruik maken van het desinfectiemiddel.

8. ~ Aangezien de situatie blijft veranderen kunnen onze maatregelen verder aangepast worden. Gemaakte reserveringen kunnen in overleg dan verplaatst of geannuleerd worden.

.. We wensen iedereen een goede gezondheid toe en veel plezier op het water!