---
title: Cadeaubon
date: 2020-04-09 19:32:00 +02:00
---

Het is ook mogelijk om cadeaubonnen te kopen bij ons. De bonnen zijn via de mail bij ons te bestellen. 

De cadeaubonnen zijn na bestelling op afspraak op te halen in Harmelen, Haanwijk 4  of in Woerden aan de Singel, 
Je krijgt dan een leuke kapiteinspet bij een bon voor een halve of een hele dag! 
Een bon voor een ander bedrag kan ook, laat het ons maar even weten;

Wanneer het plan er is om te varen kan er op de gewenste dag een reservering gemaakt te worden via de site.

De cadeaubon is het hele jaar na uitgifte geldig.


![cadeaubon met kapiteinspet-77fad7.png](/uploads/cadeaubon%20met%20kapiteinspet-77fad7.png)